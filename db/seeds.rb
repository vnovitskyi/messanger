# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Admin.create(email: 'admin@example.com', password: 'password')
User.create(email: 'user_1@example.com', password: 'password', name: 'Jonny')
User.create(email: 'user_2@example.com', password: 'password', name: 'Bruce')
User.create(email: 'user_3@example.com', password: 'password', name: 'Ali')
