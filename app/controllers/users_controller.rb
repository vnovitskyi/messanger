class UsersController < ApplicationController

  DEFAULT_PASSWORD = 'simple_the_best'

  before_filter :authenticate_any_to_update!, only: [:edit, :update]
  before_filter :authenticate_admin!, except: [:edit, :update]
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    set_default_password(@user) if params[:user][:default_password] == 'true'

    if @user.save
      redirect_to users_path, notice: 'user was successfully created.'
    else
      render :new
    end
  end

  def update
    if @user.update(user_params)
      redirect_to after_update
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    redirect_to users_url, notice: 'user was successfully destroyed.'
  end

  private

  def after_update
    admin_signed_in? ? users_path : message_path
  end

  def authenticate_any_to_update!
    if admin_signed_in?
      true
    else
      authenticate_user!
    end
  end

  def set_default_password(user)
    user.password = DEFAULT_PASSWORD
  end
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:name, :email, :password, :default_password, :banned)
  end
end
