class WelcomeController < ApplicationController
  def index
    @unread_message_count = Message.unread_for(current_user).count
  end
end
