class MessagesController < ApplicationController
  before_action :authenticate_user!

  def index
    @messages = Message.unread_for(current_user.id)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    @message = Message.new
  end

  def create
    @message = Message.new(messenger_params)
    if @message.save
      redirect_to message_path(@message.recipient_id)
    else
      render :show
    end
  end

  def show
    @message = Message.new
    @messages = Message.load_conversation(current_user.id, params[:id].to_i)

    set_messages_as_read if params[:read] == 'true'
  end

  private

  def set_messages_as_read
    Message.unread_for(current_user).update_all(read: true)
  end

  def messenger_params
    params.require(:message).permit(:body, :recipient_id).merge!(sender_id: current_user.id)
  end

end
