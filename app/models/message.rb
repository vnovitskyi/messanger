class Message < ActiveRecord::Base

  validates :body, presence: true

  scope :unread_for, ->(user_id) {where(recipient_id: user_id, read: false)}
  scope :latest_messages, ->(user_id) {where(['sender_id = ? or recipient_id = ?', user_id, user_id]).order('created_at DESC')}
  scope :load_conversation, ->(user1_id, user2_id) { where(['recipient_id = ? AND sender_id = ? OR recipient_id = ? AND sender_id = ?', user1_id, user2_id, user2_id, user1_id]).order('created_at DESC')}

  belongs_to :sender, class_name: 'User'
  belongs_to :recipient, class_name: 'User'

end
