class User < ActiveRecord::Base

  attr_accessor :default_password

  scope :all_except, ->(user) { where.not(id: user) }
  scope :not_banned, -> { where(banned: false) }

  has_many :sent_messages, class_name: 'Message', foreign_key: :sender_id
  has_many :received_messages, class_name: 'Message', foreign_key: :receiver_id

  validates :name, presence: true, format: /\A[a-zA-Z\s\'-]+\Z/

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
